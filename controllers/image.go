package controllers

import (
    "image"
    "image/color"
    "image/jpeg"
    "math"
    "os"
    "github.com/gin-gonic/gin"
    "net/http"
)

// ImageController ...
type ImageController struct {
}
// NewImageController ...
func NewImageController() ImageController {
	return ImageController{}
}

// Circle ...
type Circle struct {
    p image.Point
    r int
}

// ImageIndex ... imageトップ
func (ic ImageController) ImageIndex(c *gin.Context) {
	c.HTML(http.StatusOK, "image_index.html", gin.H{
	})
}

// ChangeImage ... ボタン押下
func (ic ImageController) ChangeImage(c *gin.Context) {
    renderImage := false
    changeImage := false
    if c.PostForm("mode") == "render" {
        imageRender()
        renderImage = true
    } else {
        imageChange()
        changeImage = true
    }
	c.HTML(http.StatusOK, "image_index.html", gin.H{
        "renderImage": renderImage,
        "changeImage": changeImage,
	})
}

// 描画する
func imageRender() {
    x := 0
    y := 0
    width := 300
	height := 300

    // RectからRGBAを作る(最初は黒)
    img := image.NewRGBA(image.Rect(x, y, width, height))
    // 白色に染める(透過なし)
    fillRect(img, color.RGBA{255, 255, 255, 0})

    // 顔
    for size := 80; size >= 0; size-- {
        faceCircle := Circle{image.Point{150, 180}, size}
        faceCircle.drawBounds(img, color.RGBA{0, 0, 0, 0})
    }
    // 耳
    for size := 45; size >= 0; size-- {
        yearCircle1 := Circle{image.Point{60, 95}, size}
        yearCircle1.drawBounds(img, color.RGBA{0, 0, 0, 0})
        yearCircle2 := Circle{image.Point{240, 95}, size}
        yearCircle2.drawBounds(img, color.RGBA{0, 0, 0, 0})
    }

    // 出力用ファイル作成
    file, _ := os.Create("image/mickey.jpg")
    defer file.Close()

    // JPEGで出力(100%品質)
    if err := jpeg.Encode(file, img, &jpeg.Options{100}); err != nil {
        panic(err)
    }
}

// 単色塗りつぶし
func fillRect(img *image.RGBA, col color.Color) {
    rect := img.Rect
    for h := rect.Min.Y; h < rect.Max.Y; h++ {
        for v := rect.Min.X; v < rect.Max.X; v++ {
            img.Set(v, h, col)
        }
    }
}

// 円を描く
func (c *Circle) drawBounds(img *image.RGBA, col color.Color) {
    for rad := 0.0; rad < 2.0*float64(c.r); rad += 0.1 {
        x := int(float64(c.p.X) + float64(c.r)*math.Cos(rad))
        y := int(float64(c.p.Y) + float64(c.r)*math.Sin(rad))
        img.Set(x, y, col)
    }
}

// グレースケール化
func imageChange() {
    // 元ファイル読み込み
    src, _ := os.Open("./image/gopher001.jpg")
    defer src.Close()

    // イメージオブジェクト作成
    srcImg, _, err := image.Decode(src)
    if err != nil {
        panic(err)
    }
    srcBounds := srcImg.Bounds()

    // 出力用イメージ
    dest := image.NewGray(srcBounds)
    // グレー化
    for v := srcBounds.Min.Y; v < srcBounds.Max.Y; v++ {
        for h := srcBounds.Min.X; h < srcBounds.Max.X; h++ {
            c := color.GrayModel.Convert(srcImg.At(h, v))
            gray, _ := c.(color.Gray)
            dest.Set(h, v, gray)
        }
    }

    // 出力用ファイル作成(エラー処理は略)
    outfile, _ := os.Create("image/gopher002.jpg")
    defer outfile.Close()
    // 書き出し
    jpeg.Encode(outfile, dest, &jpeg.Options{100})
}