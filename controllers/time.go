package controllers

import (
    "github.com/gin-gonic/gin"
	"net/http"
	"time"
)

var successFormat string

// TimeController ...
type TimeController struct {
}
// NewTimeController ...
func NewTimeController() TimeController {
	return TimeController{}
}

func init() {
	successFormat = "2006-01-02 15:04:05"
}

// TimeIndex ...
func (tc TimeController) TimeIndex(c *gin.Context) {
	c.HTML(http.StatusOK, "time_index.html", gin.H{
	})
}

// TimeFormat ...
func (tc TimeController) TimeFormat(c *gin.Context) {
	time := ""
	if c.PostForm("format") == "success" {
		time = changeSuccessFormat()
	} else {
		time = changeErrorFormat()
	}

	c.HTML(http.StatusOK, "time_index.html", gin.H{
		"time": time,
	})
}

// 正しい書式変換
func changeSuccessFormat() string {
	t := time.Now()
	return t.Format(successFormat)
}

// 間違った書式変換
func changeErrorFormat() string {
	t := time.Now()
	layout := "2018-12-31 23:59:59"
	return t.Format(layout)
}

// 現在の1ヵ月後
func nextMonth() string {
	t := time.Now()
	t = t.AddDate(0, 1, 0)
	return t.Format(successFormat)
}