package main

import (
	"log"
	"github.com/gin-gonic/gin"
	"net/http"
	"go_packages/controllers"
)

func main() {
	log.Println("test")

	router := gin.Default()

	// css読み込み
	router.Static("/css3", "./css")
	// image読み込み
	router.Static("/image", "./image")
	
	// teplateディレクトリを指定
	router.LoadHTMLGlob("templates/*.html")

	// MENU
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
		})
	})

	// time
	tc := controllers.NewTimeController()
	router.GET("/time", tc.TimeIndex)
	router.POST("/time", tc.TimeFormat)

	// image
	ic := controllers.NewImageController()
	router.GET("/image", ic.ImageIndex)
	router.POST("/image", ic.ChangeImage)

	router.Run(":9090")
}
